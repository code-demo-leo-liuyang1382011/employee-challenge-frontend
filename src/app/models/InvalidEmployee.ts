import { Employee } from './Employee';
export class InvalidEmployee extends Employee {
    constructor(name: string, subordinateList: Array<Employee>,  ceo: boolean, validEmployee: boolean) {
        super(name, subordinateList, ceo, validEmployee);
        this.level = Employee.INVALID_LEVEL;
    }

     // Override
    displayName(): string {
        return `${this.name} (Invalid)`;
    }
    
    // Override
    addSubordiate(e: Employee): void {
        // Should do nothing as invalid Employee will not have any subordinate
    }
}