import { Employee } from './Employee';
export class NormalEmployee extends Employee {
    constructor(name: string, subordinateList: Array<Employee>, ceo: boolean, validEmployee: boolean) {
        super(name, subordinateList, ceo, validEmployee);
        this.level = Employee.NORMAL_LEVEL;
        if (this.subordinateList.length > 0) {
            this.level = 'normal-manager';
        } else {
            this.level = 'normal-leaf';
        }
    }
}