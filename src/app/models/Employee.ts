export class Employee {
    name: string;
    subordinateList: Array<Employee>;
    ceo: boolean;
    validEmployee: boolean;
    expanded: boolean;
    level?: string;

    public static NORMAL_LEVEL = 'normal';
    public static INVALID_LEVEL = 'invalid';
    public static CEO_LEVEL = 'CEO';

    constructor(name: string, subordinateList: Array<Employee>, ceo: boolean, validEmployee: boolean) {
        this.expanded = true;
        this.name = name;
        this.subordinateList = subordinateList || [];
        this.ceo = ceo;
        this.validEmployee = validEmployee;
        this.level = 'abstract';
    }

    toggle() {
        this.expanded = !this.expanded;
    }

    addSubordiate(e: Employee): void {
        this.subordinateList.push(e);
    }

    displayName(): string {
        return this.name;
    }

    getLevel(): string {
        return this.level;
    }
}