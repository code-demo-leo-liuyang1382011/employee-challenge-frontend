import { Employee } from './Employee';
export class Ceo extends Employee {
    constructor(name: string, subordinateList: Array<Employee>, ceo: boolean, validEmployee: boolean) {
        super(name, subordinateList, ceo, validEmployee);
        this.level = Employee.CEO_LEVEL;
    }

    // Override
    displayName(): string {
        return `${this.name} (CEO)`;
    }

    // Override
    addManager(id: number): void {
        // Should do nothing here as CEO is assuemd to not have any manager
    }
}