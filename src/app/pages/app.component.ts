import { EmployeeService } from '../services/http/employee.service';
import { Component } from '@angular/core';
import { EmployeeFactoryService } from 'app/services/employeeFactory.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    EmployeeService
  ]
})
export class AppComponent {
  private title:string;
  public employeeTree: any;

  constructor(
    private employeeService: EmployeeService,
    private employeeFactoryService: EmployeeFactoryService 
  ) {
    this.title = 'Organisation Chart';
    this.getEmployeeMap();
  }

  public async getEmployeeMap() {
    try {
      const employeeTreeRaw = await this.employeeService.getEmployeeTree();
      this.employeeTree = this.employeeFactoryService.createEmployee(employeeTreeRaw);
    } catch (err) {
      window.alert('Failed to get the orgnisation information');
    }
  }
}
