import { Injectable } from '@angular/core';
import { Tree } from 'app/interfaces/Tree';

@Injectable()
export class TreeService {

  constructor() { }

  public transformTreeForUI(root: Tree) {
    if (!root) {
      return;
    }
    root.expanded = false;
    if (root.subordinateObjectList.length === 0) {
      return;
    }    
    for (const subordinate of root.subordinateObjectList) {
      this.transformTreeForUI(subordinate);
    }
  }
}
