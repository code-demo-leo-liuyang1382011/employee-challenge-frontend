import { ServiceHandler } from '../service.handler';
import {Injectable} from '@angular/core';
import {HttpClient} from '../http.client';

@Injectable()
export class EmployeeService {
    constructor(
        private http: HttpClient,
        private serviceHandler: ServiceHandler
    ){}

    getAllEmployee() {
      return new Promise((resolve, reject) => {
        this.http.get('employees', null)
        .subscribe((res) => {
            resolve(res);
        }, (err) => {
            let errObj = this.serviceHandler.handleError(err);
            reject(errObj);
        });
      });
    }

    getAllEmployeeById(uuid: string) {
      return new Promise((resolve, reject) => {
        this.http.get(`employees/${uuid}`, null)
        .subscribe((res) => {
            resolve(res);
        }, (err) => {
            let errObj = this.serviceHandler.handleError(err);
            reject(errObj);
        });
      });
    }

    getEmployeeTree() {
      return new Promise((resolve, reject) => {
        this.http.get('employees/tree', null)
        .subscribe((res) => {
            resolve(res);
        }, (err) => {
            let errObj = this.serviceHandler.handleError(err);
            reject(errObj);
        });
      });
    }
}