import { NormalEmployee } from '../models/NormalEmployee';
import { Ceo } from '../models/Ceo';
import { InvalidEmployee } from '../models/InvalidEmployee';
import { Employee } from '../models/Employee';
import { Injectable } from '@angular/core';

@Injectable()
export class EmployeeFactoryService {
    constructor() {}

    public createEmployee(e: any): Employee {
        let result: Employee = null;
        if (e.subordinateObjectList.length === 0) {
          return this.createEmployeeWithParams(e.name, [], e.ceo, e.validEmployee);
        }
        const subordinateList: Array<Employee> = [];
        for (const subordinteTree of e.subordinateObjectList) {
          subordinateList.push(this.createEmployee(subordinteTree));
        }
        
        return this.createEmployeeWithParams(e.name, subordinateList, e.ceo, e.validEmployee);
    }

    public createEmployeeWithParams(
      name: string, 
      subordinateList: Array<Employee>, 
      ceo: boolean, 
      validEmployee: boolean): Employee {
      let result: Employee = null;
      if (!validEmployee) {
        result = new InvalidEmployee(name, subordinateList, ceo, validEmployee);
      } else if (ceo) {
          result = new Ceo(name, subordinateList, ceo, validEmployee);
      } else {
          result = new NormalEmployee(name, subordinateList, ceo, validEmployee);
      }
      return result;
    }
}