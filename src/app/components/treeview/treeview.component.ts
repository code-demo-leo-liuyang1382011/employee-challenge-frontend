import { Component, Input, OnInit, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { Employee } from "app/models/Employee";

@Component({
	selector: 'tree-view',
    templateUrl: './treeview.component.html',
    styleUrls: ['./treeview.component.scss']
})
export class TreeViewComponent implements OnInit{
	@Input()
  public tree: Employee;

	constructor() {}

  public ngOnInit():void {
  }

  public toggle(node: Employee) {
    if (node.subordinateList.length === 0) {
      return;
    }
    for (const subordinateNode of node.subordinateList) {
      subordinateNode.expanded = !subordinateNode.expanded;
      this.toggle(subordinateNode);
    }
  }
}
