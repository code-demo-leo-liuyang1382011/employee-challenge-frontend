import { TreeViewModule } from './components/treeview/treeview.moule';
import { ServiceHandler } from './services/service.handler';
import { HttpClient } from './services/http.client';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './pages/app.component';
import { EmployeeFactoryService } from './services/employeeFactory.service';
import { TreeService } from './services/tree.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TreeViewModule
  ],
  providers: [
    HttpClient,
    ServiceHandler,
    EmployeeFactoryService,
    TreeService
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
