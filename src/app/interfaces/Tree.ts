export interface Tree {
  ceo: boolean;
  name: string;
  subordinateObjectList: Array<Tree>,
  validEmployee: boolean;
  expanded?: boolean;
}